#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "keygen.h"

extern byte R1[R1_BITS];
extern byte R2[R2_BITS];
extern byte R3[R3_BITS];
extern byte R4[R4_BITS];

unsigned int bitArrayToDecimal(const byte *a, size_t len) {
  unsigned int r = 0;
  for( size_t i = 0; i < len; ++i )
    r |= (a[i] == 1) << (i - 0);
  return r;
}

void debugRegs(void) {
  printf("R1 = %u\nR2 = %u\nR3 = %u\nR4 = %u\n",
    bitArrayToDecimal(R1, R1_BITS),
    bitArrayToDecimal(R2, R2_BITS),
    bitArrayToDecimal(R3, R3_BITS),
    bitArrayToDecimal(R4, R4_BITS)
  );

  /*
  printf("R1 = ");
  for( int i = 0; i < R1_BITS; ++i )
    printf("%u", R1[i]);
  printf("\n");

  printf("R2 = ");
  for( int i = 0; i < R2_BITS; ++i )
    printf("%u", R2[i]);
  printf("\n");

  printf("R3 = ");
  for( int i = 0; i < R3_BITS; ++i )
    printf("%u", R3[i]);
  printf("\n");

  printf("R4 = ");
  for( int i = 0; i < R4_BITS; ++i )
    printf("%u", R4[i]);
  printf("\n");
  */
}

int main(int argc, char **argv) {
  /* Kc = AAAAAAAA */
  byte Kc[SECRETKEY_BITS] = {0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0,1};
  /* FrameId = 0   */
  byte frameId[FRAMEID_BITS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte verifiedKeystream[228] = {1,1,1,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,1,0,1,1,0,0,1,0,1,0,1,1,0,0,0,0,0,1,0,0,1,1,0,1,0,1,1,0,0,1,0,0,1,1,0,1,1,1,0,1,1,0,0,1,0,0,0,1,0,0,0,1,1,0,0,0,0,0,1,0,1,1,0,1,1,1,0,0,1,0,0,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,1,0,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,0,0,1,1,0,0,1,0,1,0,0,0,1,1,1,0,0,0,0,1,0,1,1,0,1,0,1,0,0,0,0,1,0,1,0,0,1,1,0,1,1,1,0,0,1,1,0,1,0,1,1,1,1,0,1,1,1,0,0,1,0,1,1,1,0,0,1,0,0,0,1,0,0,0,1,0,0,1,1,0,0,1,0,1,0,0,0,1,0,0};
  byte keystream[228];
  memset(keystream, 0, 228*sizeof(byte));
  keysetup(Kc, frameId);
  getKeystream(keystream, 228);
  for( int i = 0; i < 228; ++ i )
    printf("%u", keystream[i]);
  printf("\n");

  return 0;
}
